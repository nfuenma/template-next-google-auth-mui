import { getServerSession } from 'next-auth/next';
import { NextApiRequest, NextApiResponse } from 'next';

const authenticatorMiddleware = async (
  req: NextApiRequest,
  res: NextApiResponse,
  next: (reqNext: NextApiRequest, resNext: NextApiResponse) => void,
): Promise<unknown> => {
  const session = await getServerSession(req, res, {});
  if (session) {
    return next(req, res);
  }
  return res.status(401).json({ message: 'unauthorized' });
};

export default authenticatorMiddleware;
