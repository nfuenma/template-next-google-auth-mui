export type ResponseCreateType<T> = {
  message: string;
  data?: T;
  error?: Error
}

export type ResponseListType<T> = {
  message: string;
  data?: Array<T>;
  error?: Error
}
