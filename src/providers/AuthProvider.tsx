import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import { PropsWithChildren } from 'react';
import SDK from '@/sdk/axios/index';
import { Stack } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';

const AuthProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const { status } = useSession();
  const { push, pathname } = useRouter();

  if (status === 'loading') {
    return (
      <Stack
        style={{ height: '100vh' }}
      >
        <CircularProgress size={50} />
      </Stack>
    );
  }

  if (status === 'unauthenticated' && pathname !== '/') push('/');

  if (status === 'authenticated') SDK.init();

  return children;
};

export default AuthProvider;
