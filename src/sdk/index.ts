import axios from 'axios';

const api = axios.create({
  baseURL: `${process.env.URL_SERVER}/api`,
});

const makeRequest = async (endpoint: string, metodo: string = 'GET', params = {}) => {
  try {
    const response = await api({
      method: metodo,
      url: endpoint,
      params,
    });

    return response.data;
  } catch (error) {
    return error;
  }
};

export default makeRequest;
