import React, { useEffect } from 'react';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import Login from '@/pages/login';

const Home = () => {
  const router = useRouter();
  const { data: session } = useSession();

  useEffect(() => {
    if (session?.user) router.push('/dashboard');
  }, [router, session]);

  return (
    <Login />
  );
};

export default Home;
