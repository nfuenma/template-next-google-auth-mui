import React, { useEffect } from 'react';
import { signIn, useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import { Stack, Button, ImageListItem } from '@mui/material';

const boxStyle: React.CSSProperties = {
  width: '100%',
  height: '100vh',
};

const Login: React.FC = () => {
  const { data: session } = useSession();
  const router = useRouter();
  useEffect(() => {
    if (session?.user) {
      router.push('/dashboard');
    }
  }, [router, session?.user]);
  const image = 'https://litivo.com/assets/images/litivo.png';
  return (
    <Stack style={boxStyle}>
      <Stack style={boxStyle}>
        <ImageListItem>
          <img
            width={200}
            style={{ padding: 20 }}
            alt="logo-litivo"
            srcSet={`${image}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
            src={`${image}?w=164&h=164&fit=crop&auto=format`}
            loading="lazy"
          />
        </ImageListItem>
        <Button
          type="submit"
          className="login-form-button"
          onClick={() => signIn()}
        >
          Log in
        </Button>
      </Stack>
    </Stack>
  );
};

export default Login;
