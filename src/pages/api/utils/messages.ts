const messages = {
  CREATED: 'Record created!',
  UPDATED: 'Record updated!',
  DELETED: 'Record deleted!',
  FOUND: 'Record found!',
  NOT_FOUND: 'Record not found!',
  ERROR: 'Error finding!',
};

export default messages;
